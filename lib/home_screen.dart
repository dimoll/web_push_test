import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String? _token;
  late StreamSubscription<String> _tokenSubs;
  @override
  void initState() {
    super.initState();
    _initFCM();
  }

  @override
  void dispose() {
    _tokenSubs.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text('WEB PUSH DEMO'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(32.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Your FCM token is:',
            ),
            Text(
              _token ?? 'Initializing...',
            ),
            TextButton(
              child: const Text('Copy FCM token'),
              onPressed: () => Clipboard.setData(
                ClipboardData(text: _token ?? ''),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _initFCM() async {
    // ignore: avoid_print, invalid_return_type_for_catch_error
    await FirebaseMessaging.instance.requestPermission().catchError(print);
    _token = await FirebaseMessaging.instance.getToken(vapidKey: publicKey);
    _tokenSubs = FirebaseMessaging.instance.onTokenRefresh.listen((event) {
      _token = event;
      if (mounted) {
        setState(() {});
      }
    });
    if (mounted) {
      setState(() {});
    }
    FirebaseMessaging.onMessage.listen((event) {
      print(event.data);
    });
  }
}

String publicKey =
    'BB1BC-Rb8OO-Um9Pjb7X57rWz-4l1Hro3mT5EYwVAguoEgimtCXm-cqPAGpH3TQD5ig0xVsLWqRrvKa9RyfVGEc';
// RSbfNOrWP13v2ntb21iMYvdUyXCYr2uqvEDa-zsDlXI