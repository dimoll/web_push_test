importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-messaging.js");

firebase.initializeApp({
  apiKey: "AIzaSyCo80cz9XvsyOSTOuLXST-fnHmgOR8Kzr4",
  authDomain: "web-test-push-project-tracy.firebaseapp.com",
  projectId: "web-test-push-project-tracy",
  storageBucket: "web-test-push-project-tracy.appspot.com",
  messagingSenderId: "1084949915132",
  appId: "1:1084949915132:web:5223dba4dfb3b18bb1fe79",
});

const messaging = firebase.messaging();

messaging.onBackgroundMessage((message) => {
  console.log("onBackgroundMessage", message);
});